#include "Shader.h"

/**
 *	@brief vraci obsah souboru jako string
 *	@param[in] name jmeno souboru
 */
std::string getTextFileString(std::string name){

	FILE* fp = fopen(name.c_str(), "rt");
	if(!fp){
		std::cerr << "Cannot load shader file" << std::endl;
		system("PAUSE");
		return false;
	}

	//std::vector<std::string> sLines;
	std::string text;
	char sLine[255];
	while(fgets(sLine, 255, fp))text.append(sLine);
	fclose(fp);

	return text;
}

/**
 *	@brief zkontroluje zda byl shader spravne zkompilovan, tiskne chyby do stderr
 *	@param[in] n_shader_object je id shaderu
 *	@param[in] p_s_shader_name je jmeno shaderu (jen pro uzivatele - aby vedel o ktery shader se jedna pokud se vytiskne nejaka chyba)
 *	@return vraci true pro uspesne zkompilovane shadery, jinak false
 */
bool CheckShader(GLuint n_shader_object, const char *p_s_shader_name)
{
	bool b_compiled;
	{
		int n_tmp;
		glGetShaderiv(n_shader_object, GL_COMPILE_STATUS, &n_tmp);
		b_compiled = n_tmp == GL_TRUE;
	}
	int n_log_length;
	glGetShaderiv(n_shader_object, GL_INFO_LOG_LENGTH, &n_log_length);
	// query status ...

	if(n_log_length > 1) {
		char *p_s_info_log;
		if(!(p_s_info_log = new(std::nothrow) char[n_log_length + 1]))
			return false;
		// alloc temp storage for log string

		glGetShaderInfoLog(n_shader_object, n_log_length, &n_log_length, p_s_info_log);
		// get log string

		printf("GLSL compiler (%s): %s\n", p_s_shader_name, p_s_info_log);
		// print info log

		delete[] p_s_info_log;
		// delete temp storage
	}
	// get info-log

	return b_compiled;
}

/**
 *	@brief zkontroluje zda byl program spravne slinkovan, tiskne chyby do stderr
 *	@param[in] n_program_object je id programu
 *	@param[in] p_s_program_name je jmeno programu (jen pro uzivatele - aby vedel o ktery program se jedna pokud se vytiskne nejaka chyba)
 *	@return vraci true pro uspesne slinkovne programy, jinak false
 */
bool CheckProgram(GLuint n_program_object, const char *p_s_program_name)
{
	bool b_linked;
	{
		int n_tmp;
		glGetProgramiv(n_program_object, GL_LINK_STATUS, &n_tmp);
		b_linked = n_tmp == GL_TRUE;
	}
	int n_length;
	glGetProgramiv(n_program_object, GL_INFO_LOG_LENGTH, &n_length);
	// query status ...

	if(n_length > 1) {
		char *p_s_info_log;
		if(!(p_s_info_log = new(std::nothrow) char[n_length + 1]))
			return false;
		// alloc temp log

		glGetProgramInfoLog(n_program_object, n_length, &n_length, p_s_info_log);
		// get info log

		printf("GLSL linker (%s): %s\n", p_s_program_name, p_s_info_log);
		// print info log

		delete[] p_s_info_log;
		// release temp log
	}
	// get info-log

	return b_linked;
}

bool Shader::compile(GLuint * n_vertex_shader, GLuint * n_fragment_shader, GLuint * n_program_object){
	std::string vertexString = getTextFileString(vertexpath);
	const char *p_s_vertex_shader = vertexString.c_str();

	std::string fragmentString = getTextFileString(fragmentpath);
	const char *p_s_fragment_shader = fragmentString.c_str();

	*n_vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(*n_vertex_shader, 1, &p_s_vertex_shader, NULL);
	glCompileShader(*n_vertex_shader);
	if(!CheckShader(*n_vertex_shader, "vertex shader"))
		return false;

	*n_fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(*n_fragment_shader, 1, &p_s_fragment_shader, NULL);
	glCompileShader(*n_fragment_shader);
	if(!CheckShader(*n_fragment_shader, "fragment shader"))
		return false;
	*n_program_object = glCreateProgram();

	glAttachShader(*n_program_object, *n_vertex_shader);
	glAttachShader(*n_program_object, *n_fragment_shader);
	// zkompiluje vertex / fragment shader, pripoji je k programu

	glBindAttribLocation(*n_program_object, 0, "v_tex");//2hodnoty vec2
	glBindAttribLocation(*n_program_object, 1, "v_pos");//3hodnoty vec3
	// nabinduje atributy (propojeni mezi obsahem VBO a vstupem do vertex shaderu)

	glBindFragDataLocation(*n_program_object, 0, "frag_color");
	// nabinduje vystupni promenou (propojeni mezi framebufferem a vystupem fragment shaderu)

	glLinkProgram(*n_program_object);
	if(!CheckProgram(*n_program_object, "program"))
		return false;
	// slinkuje program

	return true;
}

void Shader::deleteShader(GLuint * n_vertex_shader, GLuint * n_fragment_shader, GLuint * n_program_object){
	glDeleteShader(*n_vertex_shader);
	glDeleteShader(*n_fragment_shader);
	glDeleteProgram(*n_program_object);
}


bool ShaderBlur::compile(){
	vertexpath  = "Shaders/blur.vx.shader";
	fragmentpath = "Shaders/blur.fr.shader";

	if(!Shader::compile(&n_vertex_shader, &n_fragment_shader, &n_program_object))
		return false;

	//n_mvp_matrix_uniform = glGetUniformLocation(n_program_object, "t_modelview_projection_matrix");
	n_texture_sampler_uniform = glGetUniformLocation(n_program_object, "n_tex");
	//n_time_uniform = glGetUniformLocation(n_program_object, "f_time");
	pixel_size = glGetUniformLocation(n_program_object, "pixel_size");

	return true;
}

void ShaderBlur::bind(int width, int height){
	glUseProgram(n_program_object);
	{
		glUniform1i(n_texture_sampler_uniform, 0);
		glUniform3f(pixel_size, 1.0f/width, 1.0f/height, 0.0f);
	}
}

void ShaderBlur::deleteShader(){
	Shader::deleteShader(&n_vertex_shader, &n_fragment_shader, &n_program_object);
}

//pass
bool ShaderPass::compile(){
	vertexpath  = "Shaders/pass.vx.shader";
	fragmentpath = "Shaders/pass.fr.shader";
	if(!Shader::compile(&n_vertex_shader_pass, &n_fragment_shader_pass, &n_program_object_pass))
		return false;

	n_mvp_matrix_uniform_pass = glGetUniformLocation(n_program_object_pass, "t_modelview_projection_matrix");
	n_texture_sampler_uniform_pass = glGetUniformLocation(n_program_object_pass, "n_tex");
	n_depthmap_sampler_uniform_pass = glGetUniformLocation(n_program_object_pass, "depthmap");
	color_uniform_pass = glGetUniformLocation(n_program_object_pass, "color");

	return true;
}

void ShaderPass::bind(Matrix4f t_mvp, glm::vec3 color){
	glUseProgram(n_program_object_pass);
	{
		glUniformMatrix4fv(n_mvp_matrix_uniform_pass, 1, GL_FALSE, &t_mvp[0][0]);
		glUniform1i(n_texture_sampler_uniform_pass, 0);
		glUniform1i(n_depthmap_sampler_uniform_pass, 1);
		glUniform3f(color_uniform_pass, color.r, color.g, color.b);
	}
}

void ShaderPass::deleteShader(){
	Shader::deleteShader(&n_vertex_shader_pass, &n_fragment_shader_pass, &n_program_object_pass);
}

//hero
bool ShaderHero::compile(){
	vertexpath  = "Shaders/hero.vx.shader";
	fragmentpath = "Shaders/hero.fr.shader";
	if(!Shader::compile(&n_vertex_shader_hero, &n_fragment_shader_hero, &n_program_object_hero))
		return false;

	n_texture_sampler_uniform_hero = glGetUniformLocation(n_program_object_hero, "n_tex");
	n_depthmap_sampler_uniform_hero = glGetUniformLocation(n_program_object_hero, "depthmap");
	position_vector = glGetUniformLocation(n_program_object_hero, "position");
	facing_left_uniform_hero = glGetUniformLocation(n_program_object_hero, "facing_left");
	map_size_uniform_hero = glGetUniformLocation(n_program_object_hero, "map_size");

	return true;
}

void ShaderHero::bind(glm::vec2 position, bool facingLeft){
	glUseProgram(n_program_object_hero);
	{
		glUniform1i(n_texture_sampler_uniform_hero, 0);
		glUniform1i(n_depthmap_sampler_uniform_hero, 1);
		glUniform2f(position_vector, position.x, position.y);
		glUniform1i(facing_left_uniform_hero, facingLeft);
		glUniform2f(map_size_uniform_hero, MAP_SIZE_X, MAP_SIZE_Y);
	}
}

void ShaderHero::deleteShader(){
	Shader::deleteShader(&n_vertex_shader_hero, &n_fragment_shader_hero, &n_program_object_hero);
}

//velocity
bool ShaderVelocity::compile(){
	vertexpath  = "Shaders/velocity.vx.shader";
	fragmentpath = "Shaders/velocity.fr.shader";
	if(!Shader::compile(&n_vertex_shader_velocity, &n_fragment_shader_velocity, &n_program_object_velocity))
		return false;

	n_texture_sampler_uniform_velocity = glGetUniformLocation(n_program_object_velocity, "n_tex");//r-height,g-y velocity,b-x velocity
	n_depthmap_sampler_uniform = glGetUniformLocation(n_program_object_velocity, "depthmap");
	pixel_size = glGetUniformLocation(n_program_object_velocity, "pixel_size");

	return true;
}

void ShaderVelocity::bind(int width, int height){
	glUseProgram(n_program_object_velocity);
	{
		glUniform1i(n_texture_sampler_uniform_velocity, 0);
		glUniform1i(n_depthmap_sampler_uniform, 1);
		glUniform3f(pixel_size, 1.0f/width, 1.0f/height, 0.0f);
	}
}

void ShaderVelocity::deleteShader(){
	Shader::deleteShader(&n_vertex_shader_velocity, &n_fragment_shader_velocity, &n_program_object_velocity);
}


//height
bool ShaderHeight::compile(){
	vertexpath  = "Shaders/height.vx.shader";
	fragmentpath = "Shaders/height.fr.shader";
	if(!Shader::compile(&n_vertex_shader_height, &n_fragment_shader_height, &n_program_object_height))
		return false;

	n_texture_sampler_uniform_height = glGetUniformLocation(n_program_object_height, "n_tex");//r-height,g-y velocity v,b-x velocity u
	n_depthmap_sampler_uniform_height = glGetUniformLocation(n_program_object_height, "depthmap");
	pixel_size = glGetUniformLocation(n_program_object_height, "pixel_size");

	return true;
}

void ShaderHeight::bind(int width, int height){
	glUseProgram(n_program_object_height);
	{
		glUniform1i(n_texture_sampler_uniform_height, 0);
		glUniform1i(n_depthmap_sampler_uniform_height, 1);
		glUniform3f(pixel_size, 1.0f/width, 1.0f/height, 0.0f);
	}
}

void ShaderHeight::deleteShader(){
	Shader::deleteShader(&n_vertex_shader_height, &n_fragment_shader_height, &n_program_object_height);
}

//bod
bool ShaderPoint::compile(){
	vertexpath  = "Shaders/point.vx.shader";
	fragmentpath = "Shaders/point.fr.shader";
	if(!Shader::compile(&n_vertex_shader_point, &n_fragment_shader_point, &n_program_object_point))
		return false;

	return true;
}

void ShaderPoint::bind(){
	glUseProgram(n_program_object_point);
	{
	}
}

void ShaderPoint::deleteShader(){
	Shader::deleteShader(&n_vertex_shader_point, &n_fragment_shader_point, &n_program_object_point);
}

//bod simple
bool ShaderPointSimple::compile(){
	vertexpath  = "Shaders/pointSimple.vx.shader";
	fragmentpath = "Shaders/pointSimple.fr.shader";
	if(!Shader::compile(&n_vertex_shader_pointSimple, &n_fragment_shader_pointSimple, &n_program_object_pointSimple))
		return false;

	map_size_uniform_pointSimple = glGetUniformLocation(n_program_object_pointSimple, "map_size");

	return true;
}

void ShaderPointSimple::bind(){
	glUseProgram(n_program_object_pointSimple);
	{
		glUniform2f(map_size_uniform_pointSimple, MAP_SIZE_X, MAP_SIZE_Y);
	}
}

void ShaderPointSimple::deleteShader(){
	Shader::deleteShader(&n_vertex_shader_pointSimple, &n_fragment_shader_pointSimple, &n_program_object_pointSimple);
}


//boxes
bool ShaderBoxes::compile(){
	vertexpath  = "Shaders/boxes.vx.shader";
	fragmentpath = "Shaders/boxes.fr.shader";
	if(!Shader::compile(&n_vertex_shader_boxes, &n_fragment_shader_boxes, &n_program_object_boxes))
		return false;

	color_unifrom_boxes = glGetUniformLocation(n_program_object_boxes, "color");
	map_size_uniform_boxes = glGetUniformLocation(n_program_object_boxes, "map_size");

	return true;
}

void ShaderBoxes::bind(glm::vec3 color){
	glUseProgram(n_program_object_boxes);
	{
		glUniform3f(color_unifrom_boxes, color.r, color.g, color.b);
		glUniform2f(map_size_uniform_boxes, MAP_SIZE_X, MAP_SIZE_Y);
	}
}

void ShaderBoxes::deleteShader(){
	Shader::deleteShader(&n_vertex_shader_boxes, &n_fragment_shader_boxes, &n_program_object_boxes);
}

//krivka
bool ShaderSurface::compile(std::string vertexfilepath, std::string fragmentfilepath){
	vertexpath  = vertexfilepath;
	fragmentpath = fragmentfilepath;
	if(!Shader::compile(&n_vertex_shader_curve, &n_fragment_shader_curve, &n_program_object_curve))
		return false;

	point1_uniform_curve = glGetUniformLocation(n_program_object_curve, "point1");
	point2_uniform_curve = glGetUniformLocation(n_program_object_curve, "point2");

	amplitude_uniform_curve = glGetUniformLocation(n_program_object_curve, "amplitude");
	frequency_uniform_curve = glGetUniformLocation(n_program_object_curve, "frequency");
	phase_uniform_curve = glGetUniformLocation(n_program_object_curve, "phase");

	color_uniform_curve = glGetUniformLocation(n_program_object_curve, "color");

	map_size_uniform = glGetUniformLocation(n_program_object_curve, "map_size");

	return true;
}

void ShaderSurface::bind(glm::vec2 point1, glm::vec2 point2, float amplitude, float frequency, float phase, glm::vec3 color){
	glUseProgram(n_program_object_curve);
	{
		glUniform2f(point1_uniform_curve, point1.x, point1.y);
		glUniform2f(point2_uniform_curve, point2.x, point2.y);

		glUniform1f(amplitude_uniform_curve, amplitude);
		glUniform1f(frequency_uniform_curve, frequency);
		glUniform1f(phase_uniform_curve, phase);

		glUniform3f(color_uniform_curve, color.r, color.g, color.b);
		glUniform2f(map_size_uniform, MAP_SIZE_X, MAP_SIZE_Y);
	}
}

void ShaderSurface::deleteShader(){
	Shader::deleteShader(&n_vertex_shader_curve, &n_fragment_shader_curve, &n_program_object_curve);
}